package imobile;


import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import imobile.resources.CrawlerResource;
import imobile.service.CrawlerService;

public class ImobileCrawlerApplication extends Application<ImobileCrawlerConfiguration> {

    public static void main(final String[] args) throws Exception {
        new ImobileCrawlerApplication().run(args);
    }

    @Override
    public String getName() {
        return "imobile-crawler";
    }

    @Override
    public void initialize(final Bootstrap<ImobileCrawlerConfiguration> bootstrap) {
    }

    @Override
    public void run(final ImobileCrawlerConfiguration configuration,
                    final Environment environment) {
        final ImobileCrawlController crawlController = new ImobileCrawlController(configuration);
        final CrawlerService crawlerService = new CrawlerService(crawlController);
        final CrawlerResource crawlerResource = new CrawlerResource(crawlerService);
        environment.jersey().register(crawlerResource);
    }

}
