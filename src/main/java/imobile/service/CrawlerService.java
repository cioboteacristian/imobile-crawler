package imobile.service;

import imobile.ImobileCrawlController;

public class CrawlerService {
  private final ImobileCrawlController imobileCrawlController;

  public CrawlerService(final ImobileCrawlController imobileCrawlController) {
    this.imobileCrawlController = imobileCrawlController;
  }

  public void startCrawler(final String crawlerName) throws Exception {
    this.imobileCrawlController.startCrawler(crawlerName);
  }
}
