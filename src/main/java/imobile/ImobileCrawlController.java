package imobile;

import java.util.HashMap;
import java.util.Map;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import imobile.crawler.ImobileNetCrawler;

import static edu.uci.ics.crawler4j.crawler.CrawlController.WebCrawlerFactory;

public class ImobileCrawlController {

  private ImobileCrawlerConfiguration configuration;

  public ImobileCrawlController(final ImobileCrawlerConfiguration configuration) {
    this.configuration = configuration;
  }

  public void startCrawler(final String crawlerName) throws Exception {
    CrawlController crawlController = createCrawlController();
    Map<String, WebCrawlerFactory<? extends WebCrawler>> crawlers = createWebCrawlers();
    crawlController.start(crawlers.get(crawlerName.toLowerCase()), this.configuration.numberOfCrawlers);
    crawlController.shutdown();
  }

  private CrawlController createCrawlController() throws Exception {
    final CrawlConfig crawlConfig = createCrawlConfig(configuration);
    final PageFetcher pageFetcher = createPageFetcher(crawlConfig);
    final RobotstxtServer robotstxtServer = createRobotstxtServer(pageFetcher);
    final CrawlController controller = new CrawlController(crawlConfig, pageFetcher, robotstxtServer);
    this.configuration.crawlerSeeds.forEach(controller::addSeed);
    return controller;
  }

  private RobotstxtServer createRobotstxtServer(final PageFetcher pageFetcher) {
    final RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
    return new RobotstxtServer(robotstxtConfig, pageFetcher);
  }

  private PageFetcher createPageFetcher(final CrawlConfig crawlConfig) {
    return new PageFetcher(crawlConfig);
  }

  private CrawlConfig createCrawlConfig(final ImobileCrawlerConfiguration configuration) {
    final CrawlConfig crawlerConfig = new CrawlConfig();
    crawlerConfig.setCrawlStorageFolder(configuration.crawlStorageFolder);
    crawlerConfig.setMaxPagesToFetch(configuration.crawlerMaxPagesToFetch);
    crawlerConfig.setPolitenessDelay(configuration.crawlerDelay);
    crawlerConfig.setMaxDepthOfCrawling(configuration.crawlerMaxDepthOfCrawling);
    return crawlerConfig;
  }

  private static Map<String, WebCrawlerFactory<? extends WebCrawler>> createWebCrawlers() {
    final Map<String, WebCrawlerFactory<? extends WebCrawler>> crawlers = new HashMap<>();
    final ImobileNetCrawler.CrawlerFactory imobileNetCrawlerFactory = new ImobileNetCrawler.CrawlerFactory(new HashMap<>());
    crawlers.put("imobilenet", imobileNetCrawlerFactory);
    return crawlers;
  }
}
