package imobile.crawler;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.UUID;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;

import imobile.core.Anunt;
import imobile.core.Pret;

public class ImobileNetScraping {

  public static Anunt scrap(String url, String html) {
    Document document = Jsoup.parse(html);
    UUID id = UUID.randomUUID();
    LocalDate dataExtragere = LocalDate.now();
    String anConstructie = scrapAnConstructie(document);
    String suprafata = scrapSuprafata(document);
    Pret pret = scrapPret(document);
    LocalDate dataPublicarii = scrapDataPublicare(document);
    String localitate = scrapLocalitate(document);
    String adresa = scrapAdresa(document);
    String descriere = scrapDescriere(document);
    String externalId = scrapExternalId(document);
    return new Anunt(id, externalId, dataPublicarii, dataExtragere, localitate, adresa, pret.valoare, pret.moneda,
        suprafata, anConstructie, url, descriere);
  }

  private static String scrapExternalId(final Document document) {
    String element = document.getElementsByClass("post")
        .first()
        .getElementsByClass("colorat")
        .first()
        .toString();
    int start = element.lastIndexOf("ID anunt:");
    int stop = element.lastIndexOf(")");

    if (start < 0 ||
        start + 16 > stop) {
      return "";
    }

    return element.substring(start + 15, stop - 1)
        .replaceAll("\\s+","");
  }

  private static String scrapDescriere(final Document document) {
    String element = document.getElementsByClass("post")
        .first()
        .getElementsByClass("colorat")
        .first()
        .toString();
    int start = element.lastIndexOf("<br clear=\"all\">");
    int stop = element.lastIndexOf("(");

    if (stop < 0) {
      stop = element.lastIndexOf("</td>");
    }

    if (start < 0 ||
        start + 16 > stop) {
      return "";
    }

    return element.substring(start + 17, stop);
  }

  private static String scrapAdresa(final Document document) {
    String element = document.getElementsByClass("post")
        .first()
        .getElementsByClass("box_shadow")
        .first()
        .toString();

    if (element.isEmpty()) {
      return "";
    }

    int start = element.lastIndexOf(",");
    int stop = element.lastIndexOf("</div>");

    if (start < 0 ||
        start + 2 > stop) {
      return "";
    }

    String adresa = element
        .substring(start + 2, stop -2);

    if (adresa.contains("</tr>")) {
      return "";
    }

    return adresa.replaceAll("<b>|</b>|\n", "");
  }

  private static String scrapLocalitate(final Document document) {
    String lastElement = document.getElementsByClass("post")
        .first()
        .getElementsByClass("box_shadow")
        .first()
        .getAllElements()
        .last()
        .toString();
    if (lastElement.contains("<div")) {
      return "";
    }
    return lastElement.replaceAll("<b>|</b>|\n", "");
  }

  private static LocalDate scrapDataPublicare(final Document document) {
    Elements elements = document.getElementsMatchingOwnText("Publicat la");
    if (elements.isEmpty()) {
      return null;
    }
    Element element = elements.first();
    if (element == null) {
      return null;
    }
    int start = element.toString()
        .lastIndexOf("Publicat la ");

    String dataPublicariiRaw = element.toString()
        .substring(start)
        .replaceAll("\\D+","");

    final DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
        .appendPattern("ddMMyyyy")
        .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
        .toFormatter()
        .withZone(ZoneId.of("Europe/Amsterdam"));
    return dateTimeFormatter.parse(dataPublicariiRaw, LocalDate::from);
  }

  private static Pret scrapPret(final Document document) {
    Elements elements = document.getElementsMatchingOwnText("EURO|LEI");

    if (elements.isEmpty()) {
      return new Pret();
    }
    Element element = elements.last();
    if (element == null) {
      return new Pret();
    }

    String pretValoare = element.toString().replaceAll("\\D+","");
    String pretMoneda = element.toString().replaceAll("[^EURO|LEI]+","");

    return new Pret(pretValoare, pretMoneda);
  }

  private static String scrapSuprafata(final Document document) {
    Elements elements = document.getElementsByAttributeValueContaining("title","Suprafata");
    if (elements.isEmpty()) {
      return "";
    }
    Element element = elements.first();
    if (element == null) {
      return "";
    }
    return element.toString().replaceAll("\\D+","");
  }


  private static String scrapAnConstructie(Document document) {
    Elements elements = document.getElementsByAttributeValueContaining("title","An constructie");
    if (elements.isEmpty()) {
      return "";
    }
    // TODO: log if multiple elements
    Element element = elements.first();
    if (element == null) {
      return "";
    }
    return element.toString().replaceAll("\\D+","");
  }
}
