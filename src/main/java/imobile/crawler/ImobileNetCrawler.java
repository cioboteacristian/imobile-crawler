package imobile.crawler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import edu.uci.ics.crawler4j.crawler.CrawlController.WebCrawlerFactory;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import imobile.core.Anunt;

public class ImobileNetCrawler extends WebCrawler {
  private final Map<String, String> metadata;
  private CSVWriter csvWriter;
  private StatefulBeanToCsv<Anunt> sbc;
  private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg"
      + "|png|mp3|mp4|zip|gz))$");

  public ImobileNetCrawler(final Map<String, String> metadata) {
    this.metadata = metadata;
  }

  @Override
  public boolean shouldVisit(final Page referringPage, final WebURL url) {
    // default implementation - no follow links
    if (!super.shouldVisit(referringPage, url)) {
      return false;
    }

    String href = url.getURL().toLowerCase();

    if (FILTERS.matcher(href).matches()) {
      return false;
    }

    if (href.contains("inchirieri")) {
      return false;
    }

    if (href.contains("terenuri")) {
      return false;
    }

    // in special anunt si oferta
    return  href.startsWith("http://www.imobiliare.net/oferta/")
        || href.startsWith("http://www.imobiliare.net/anunt/") ||
        href.contains("rank-sus");
  }

  @Override
  public void visit(final Page page) {
    if (!shouldScrapPage(page)) {
      return;
    }

    String url = page.getWebURL().getURL();

    HtmlParseData html = (HtmlParseData) page.getParseData();
    Anunt anunt = ImobileNetScraping.scrap(url, html.getHtml());

    try {
      sbc.write(anunt);
    } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
      e.printStackTrace();
    }
  }

  private boolean shouldScrapPage(final Page page) {
    String url = page.getWebURL().getURL();
    if (url.contains("rank-sus") ||
        url.contains("cautare-avansata")) {
      return false;
    }

    if (url.equals("http://www.imobiliare.net/")) {
      return false;
    }

    if (!(page.getParseData() instanceof HtmlParseData)) {
      return false;
    }

    return true;
  }

  @Override
  public void onStart() {
    try {
      File file = new File("data.csv");
      csvWriter = new CSVWriter(new FileWriter(file, true));
      sbc = new StatefulBeanToCsvBuilder<Anunt>(csvWriter)
          .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
          .build();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onBeforeExit() {
    try {
      csvWriter.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static class CrawlerFactory implements WebCrawlerFactory<ImobileNetCrawler> {
    private final Map<String, String> metadata;

    public CrawlerFactory(final Map<String, String> metadata) {
      this.metadata = metadata;
    }

    @Override
    public ImobileNetCrawler newInstance() {
      return new ImobileNetCrawler(metadata);
    }
  }
}
