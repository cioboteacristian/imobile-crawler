package imobile.resources;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import imobile.service.CrawlerService;

@Path("/crawler")
@Produces("application/json")
public class CrawlerResource {
  private final CrawlerService crawlerService;

  public CrawlerResource(final CrawlerService crawlerService) {
    this.crawlerService = crawlerService;
  }

  @GET
  public Response getCrawlers() {
    return Response
        .ok("Da", MediaType.APPLICATION_JSON)
        .build();
  }

  @POST
  @Path("{crawlerName}")
  public Response start(@PathParam("crawlerName") final String crawlerName) throws Exception {
    crawlerService.startCrawler(crawlerName);
    return Response
        .ok("Crawler " + crawlerName + " has started.")
        .build();
  }
}
