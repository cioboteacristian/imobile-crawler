package imobile;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;

public class ImobileCrawlerConfiguration extends Configuration {

  @JsonProperty
  public List<String> crawlerSeeds;
  @JsonProperty
  public String crawlStorageFolder;
  @JsonProperty
  public int numberOfCrawlers;
  @JsonProperty
  public int crawlerDelay;
  @JsonProperty
  public int crawlerMaxPagesToFetch;
  @JsonProperty
  public int crawlerMaxDepthOfCrawling;
}
