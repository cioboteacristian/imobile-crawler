package imobile.core;

import java.time.LocalDate;
import java.util.UUID;

import com.opencsv.bean.CsvBindByName;

public class Anunt {
  @CsvBindByName(column = "id")
  public final UUID id;

  @CsvBindByName(column = "external_id")
  private final String externalId;

  @CsvBindByName(column = "data_publicare")
  private final LocalDate dataPublicare;

  @CsvBindByName(column = "data_extragere")
  private final LocalDate dataExtragere;

  @CsvBindByName(column = "localitate")
  private final String localitate;

  @CsvBindByName(column = "adresa")
  private final String adresa;

  @CsvBindByName(column = "pret_valoare")
  private final String pretValoare;

  @CsvBindByName(column = "pret_moneda")
  private final String pretMoneda;

  @CsvBindByName(column = "suprafata")
  private final String suprafata;

  @CsvBindByName(column = "an_constructie")
  private final String anConstructie;

  @CsvBindByName(column = "url")
  private final String url;

  @CsvBindByName(column = "descriere")
  private final String descriere;

  public Anunt(final UUID id, final String externalId, final LocalDate dataPublicare, final LocalDate dataExtragere, final String localitate, final String adresa,
               final String pretValoare, final String pretMoneda, final String suprafata, final String anConstructie, final String url, final String descriere) {
    this.id = id;
    this.externalId = externalId;
    this.dataPublicare = dataPublicare;
    this.dataExtragere = dataExtragere;
    this.localitate = localitate;
    this.adresa = adresa;
    this.pretValoare = pretValoare;
    this.pretMoneda = pretMoneda;
    this.suprafata = suprafata;
    this.anConstructie = anConstructie;
    this.url = url;
    this.descriere = descriere;
  }

  public UUID getId() {
    return id;
  }

  public String getExternalId() {
    return externalId;
  }

  public LocalDate getDataPublicare() {
    return dataPublicare;
  }

  public LocalDate getDataExtragere() {
    return dataExtragere;
  }

  public String getLocalitate() {
    return localitate;
  }

  public String getAdresa() {
    return adresa;
  }

  public String getPretValoare() {
    return pretValoare;
  }

  public String getPretMoneda() {
    return pretMoneda;
  }

  public String getSuprafata() {
    return suprafata;
  }

  public String getAnConstructie() {
    return anConstructie;
  }

  public String getUrl() {
    return url;
  }

  public String getDescriere() {
    return descriere;
  }
}
