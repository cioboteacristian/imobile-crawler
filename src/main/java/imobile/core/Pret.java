package imobile.core;

public class Pret {
  public final String valoare;
  public final String moneda;

  public Pret() {
    this.valoare = "";
    this.moneda = "";
  }


  public Pret(final String valoare, final String moneda) {
    this.valoare = valoare;
    this.moneda = moneda;
  }
}
